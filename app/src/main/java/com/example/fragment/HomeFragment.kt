package com.example.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {

    interface HomeFragmentProtocol {
        fun maSuperFunction()
    }

    private val viewModel:HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
         btn_hello.setOnClickListener { onButtonClick() }
            viewModel.loadedShow.observe(viewLifecycleOwner, Observer { show -> updateDisplay(show) })
    }

    private fun updateDisplay(show: Show?) {
        if (show != null) {
            textView.text = show.name
        } else {
            textView.text = " "
        }
    }

    private fun onButtonClick() {
     //   findNavController().navigate(R.id.action_homeFragment_to_settingsFragment)
     //   val directions = HomeFragmentDirections.actionHomeFragmentToSettingsFragment("hello")
     //   findNavController().navigate(directions)
        viewModel.startLoadingShow()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val activity = activity as? HomeFragmentProtocol?: return
        activity.maSuperFunction()
    }
}