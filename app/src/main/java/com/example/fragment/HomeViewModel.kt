package com.example.fragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel:ViewModel () {
    val loadedShow = MutableLiveData<Show> ()

    fun startLoadingShow() {
        val show = Show("Silicon Valley", "Comedy")
        loadedShow.value = show
    }
}